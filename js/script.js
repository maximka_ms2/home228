// Функція для анімації каточок.
// Застосувати її після завантаження всіх каточок на сторінку.
function anim() {
    let x;
    let $cards = $(".card");
    let $style = $("style");

    $cards
        .on("mousemove touchmove", function (e) {
            let pos = [e.offsetX, e.offsetY];
            e.preventDefault();
            if (e.type === "touchmove") {
                pos = [e.touches[0].clientX, e.touches[0].clientY];
            }
            let $card = $(this);
            let l = pos[0];
            let t = pos[1];
            let h = $card.height();
            let w = $card.width();
            let px = Math.abs(Math.floor(100 / w * l) - 100);
            let py = Math.abs(Math.floor(100 / h * t) - 100);
            let pa = (50 - px) + (50 - py);
            let lp = (50 + (px - 50) / 1.5);
            let tp = (50 + (py - 50) / 1.5);
            let px_spark = (50 + (px - 50) / 7);
            let py_spark = (50 + (py - 50) / 7);
            let p_opc = 20 + (Math.abs(pa) * 1.5);
            let ty = ((tp - 50) / 2) * -1;
            let tx = ((lp - 50) / 1.5) * .5;
            let grad_pos = `background-position: ${lp}% ${tp}%;`
            let sprk_pos = `background-position: ${px_spark}% ${py_spark}%;`
            let opc = `opacity: ${p_opc / 100};`
            let url = $card[0].style.backgroundImage;
            let tf = `
                transform: rotateX(${ty}deg) rotateY(${tx}deg);
                background-image: ${url};
            `;
            let style = `
                .card:hover:before { ${grad_pos} }  /* gradient */
                .card:hover:after { ${sprk_pos} ${opc} }   /* sparkles */ 
            `;
            $cards.removeClass("active");
            $card.removeClass("animated");
            $card.attr("style", tf);
            $style.html(style);
            if (e.type === "touchmove") {
                return false;
            }
            clearTimeout(x);
        }).on("mouseout touchend touchcancel", function () {
            let $card = $(this);
            $style.html("");
            const url = $card[0].style.backgroundImage;
            const bg = `background-image: ${url};`;
            $card.attr("style", bg);

            x = setTimeout(function () {
                $card.addClass("animated");
            }, 2500);
        });
}

// url API для отримання першої сторінки із каточками
// кількість карточок - 250
// https://api.pokemontcg.io/v2/cards/

// url API для отримання покемонів із певним типом
// https://api.pokemontcg.io/v2/cards?q=types: + 'тип'

// Список всіх типів покемонів в API (регістр букв важливий)
// "Colorless"
// "Darkness"
// "Dragon"
// "Fairy"
// "Fighting"
// "Fire"
// "Grass"
// "Lightning"
// "Metal"
// "Psychic"
// "Water"=

let currentPage = 1;
let pageSize = $('.controls-select').val();
let currentType = 'all';

let dataParams = {
    page: currentPage,
    pageSize: pageSize
}

const $loader = $('.loader');
const $cardsField = $('<div class="main-card-wrapper"></div>');
$cardsField.appendTo('.main-card-wrapper');

function clearCards() {
  $cardsField.empty();
}

function cardsRender(data) {
    data.forEach(function (element) {
        const $card = $('<div class="card animated"></div>');
        $card.addClass(element.types[0]);
        $card.css('backgroundImage', `url('${element.images.large}')`);
        $card.appendTo($cardsField);
    });
}

function getCards() {
    /*if (dataParams.type == "all") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards/',
        data: dataParams,
        success: function (data) {
            $loader.hide();
            cardsRender(data.data);
            anim();
        }
    });
}


function getFireCards() {
    /*if (dataParams.type == "fire") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Fire',
        data: dataParams,
        success: function (dataFire) {
            $loader.hide();
            cardsRender(dataFire.data);
            anim();
        }
    });
}


function getColorlessCards() {
    /*if (dataParams.type == "colorless") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Colorless',
        data: dataParams,
        success: function (dataColorless) {
            $loader.hide();
            cardsRender(dataColorless.data);
            anim();
        }
    });
}


function getDarknessCards() {
    /*if (dataParams.type == "darkness") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Darkness',
        data: dataParams,
        success: function (dataDarkness) {
            $loader.hide();
            cardsRender(dataDarkness.data);
            anim();
        }
    });
}


function getDragonCards() {
    /*if (dataParams.type == "dragon") {
       delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Dragon',
        data: dataParams,
        success: function (dataDragon) {
            $loader.hide();
            cardsRender(dataDragon.data);
            anim();
        }
    });
}


function getFairyCards() {
    /*if (dataParams.type == "fairy") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Fairy',
        data: dataParams,
        success: function (dataFairy) {
            $loader.hide();
            cardsRender(dataFairy.data);
            anim();
        }
    });
}


function getFightingCards() {
    /* if (dataParams.type == "fighting") {
       delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Fighting',
        data: dataParams,
        success: function (dataFighting) {
            $loader.hide();
            cardsRender(dataFighting.data);
            anim();
        }
    });
}


function getGrassCards() {
    /*if (dataParams.type == "grass") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Grass',
        data: dataParams,
        success: function (dataGrass) {
            $loader.hide();
            cardsRender(dataGrass.data);
            anim();
        }
    });
}


function getLightningCards() {
    /*if (dataParams.type == "lightning") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Lightning',
        data: dataParams,
        success: function (dataLightning) {
            $loader.hide();
            cardsRender(dataLightning.data);
            anim();
        }
    });
}


function getMetalCards() {
    /*if (dataParams.type == "metal") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Metal',
        data: dataParams,
        success: function (dataMetal) {
            $loader.hide();
            cardsRender(dataMetal.data);
            anim();
        }
    });
}


function getPsychicCards() {
     /*if (dataParams.type == "psychic") {
       delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Psychic',
        data: dataParams,
        success: function (dataPsychic) {
            $loader.hide();
            cardsRender(dataPsychic.data);
            anim();
        }
    });
}


function getWaterCards() {
    /*if (dataParams.type == "water") {
      delete dataParams.type;
    }*/
    clearCards();
    $.ajax({
        url: 'https://api.pokemontcg.io/v2/cards?q=types:Water',
        data: dataParams,
        success: function (dataWater) {
            $loader.hide();
            cardsRender(dataWater.data);
            anim();
        }
    });
}

$(document).ready(function () {
  $("#types").change(function () {
    currentType = $(this).val();
    switch (currentType) {
      case "all":
        getCards();
        break;
      case "fire":
        getFireCards();
        break;
      case "colorless":
        getColorlessCards();
        break;
      case "darkness":
        getDarknessCards();
        break;
      case "dragon":
        getDragonCards();
        break;
      case "fairy":
        getFairyCards();
        break;
      case "fighting":
        getFightingCards();
        break;
      case "grass":
        getGrassCards();
        break;
      case "lightning":
        getLightningCards();
        break;
      case "metal":
        getMetalCards();
        break;
      case "psychic":
        getPsychicCards();
        break;
      case "water":
        getWaterCards();
        break;
      default:
        getCards();
        break;
    }
  });
});

// Завантаження карточок за замовчуванням при завантаженні сторінки
getCards();

// url API для отримання першої сторінки із каточками
// кількість карточок - 250
// https://api.pokemontcg.io/v2/cards/

// url API для отримання покемонів із певним типом
// https://api.pokemontcg.io/v2/cards?q=types: + 'тип'

// Список всіх типів покемонів в API (регістр букв важливий)
// "Colorless"
// "Darkness"
// "Dragon"
// "Fairy"
// "Fighting"
// "Fire"
// "Grass"
// "Lightning"
// "Metal"
// "Psychic"
// "Water"
